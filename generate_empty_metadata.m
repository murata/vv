%%
clc;
data = struct;
data.name = 'VV00';
data.fnames = {'', ''};
I = eye(3);
data.camera = stereoParameters(cameraParameters, cameraParameters, I, zeros(3, 1));
data.passes = [];
varnames = {'Intrinsics Calib', ... 1
            'Extrinsics Calib', ... 2
            'Fundamental Calc', ... 3
            'Passes Marked', ... 4
            'Frames Extracted', ... 5
            'Frames Undistorted', ... 6
            'Detection', ... 7
            'Correction', ... 8
            'Triangulation', ... 9
            'Visualization'};
vartypes = {'double', 'double', 'double', 'double', 'double', 'double', 'double', 'double', 'double', 'double'};
data.tasks = table('Size', [1, 10], 'VariableTypes', vartypes, 'VariableNames', varnames);
data.detection_scales = [0.0625, 0.125, 0.25, 0.5];
clear vartype varnames I;
uisave('data', ['data/', data.name, '.mat']);