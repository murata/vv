function [poses_scaled, heatmaps, params] = detect(net,netInput, img_size)
    heatmaps = predict(net,netInput,"Outputs","node_147");
    heatmaps = extractdata(heatmaps);
    heatmaps = heatmaps(:,:,1:end-1);
    pafs = predict(net,netInput,"Outputs","node_150");
    pafs = extractdata(pafs);
    params = getBodyPoseParameters;
    poses = getBodyPoses(heatmaps,pafs,params);
    poses_scaled = zeros(size(poses));
    
    [x,y] = convert_coords(poses(:, :, 1), poses(:, :, 2), ...
                           img_size(1), img_size(2), ...
                           size(heatmaps,1), size(heatmaps, 2));
    poses_scaled(:, :, 1) = x;
    poses_scaled(:, :, 2) = y;
end

