function [net] = load_openpose()
    modelfile = 'third_party/human-pose-estimation.onnx';
    warning('off', 'nnet_cnn_onnx:onnx:MOLayers');
    layers = importONNXLayers(modelfile,"ImportWeights",true);
    layers = removeLayers(layers,["Output_node_95" "Output_node_98" "Output_node_147" "Output_node_150"]);
    net = dlnetwork(layers);
end

