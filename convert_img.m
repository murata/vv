function [netInput] = convert_img(im)
    netInput = im2single(im)-0.5;
    netInput = netInput(:,:,[3 2 1]);
    netInput = dlarray(netInput,"SSC");
end

