function frame = load_frame(fname, idx)
    video = VideoReader(fname);
    frame = read(video, idx);
end

