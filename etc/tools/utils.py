import os
from collections import OrderedDict
import json
import cv2
import numpy as np


class utils(object):
    """Provide utility tools as all static methods."""

    def __init__(self):
        super(utils, self).__init__()

    @staticmethod
    def find_file(file_id, experiment):
        for file in experiment["files"]:
            if file_id == file["id"]:
                return file
        raise Exception("File does not exist in the experiment metadata!")

    @staticmethod
    def create_experiment_metadata(file_prefix="GOPR", file_extension=".MP4",
                                   number_of_digits=4, left_folder="left",
                                   right_folder="right", files=list()):
        """Create a metadata for the whole experiment."""
        metadata = OrderedDict(
            {
                "io": {
                    "file_prefix": file_prefix,
                    "file_extension": file_extension,
                    "number_of_digits": number_of_digits,
                    "left_folder": left_folder,
                    "right_folder": right_folder
                },
                "files": files
            }
        )
        return metadata

    @staticmethod
    def create_file_metadata_empty(id, video_file, camera, corresponding_id,
                                   run, batch, trial,
                                   anchor_frame, total_frames, IMAC38=False,
                                   people=list(), label=None):
        """Create metadata for each video file."""
        metadata = OrderedDict({"id": id,
                                "file": video_file,
                                "cam": camera,
                                "label": label,
                                "IMAC38": IMAC38,
                                "corresponding": corresponding_id,
                                "video": {
                                    "anchor_frame": anchor_frame,
                                    "total_frames": total_frames,
                                },
                                "people": people,
                                "experiment": {
                                    "run": run,
                                    "batch": batch,
                                    "trial": trial}
                                })
        return metadata

    @staticmethod
    def create_file_metadata(fname):
        id = utils.video_id_from_filename(os.path.basename(fname))
        run = 0
        batch = 0
        trial = 0
        camera = os.path.dirname(fname)
        anchor_frame, total_frames, label = utils.get_video_properties(fname)
        run, batch, trial, corresponding_id = label
        return utils.create_file_metadata_empty(id, fname, camera,
                                                corresponding_id, run, batch,
                                                trial, anchor_frame,
                                                total_frames)

    @staticmethod
    def get_video_properties(fname):
        id = utils.video_id_from_filename(os.path.basename(fname))
        video = cv2.VideoCapture(fname)
        total_frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
        corresponding_id, anchor_frame, run, batch, trial = utils.hardcoded_values(id)  # noqa: E501
        return anchor_frame, total_frames, [run, batch, trial, corresponding_id]  # noqa: E501

    @staticmethod
    def check_file_exist(arg):
        pass

    @staticmethod
    def create_folder(fname):
        """Create an empty folder."""
        os.makedirs(fname)

    @staticmethod
    def check_folder_exists(folder):
        """Check if folder given in the argument exists or not."""
        return os.path.isdir(folder)

    @staticmethod
    def files_in_folder(folder, ext=".MP4"):
        file_list = list()
        for root, _, files in os.walk(folder):
            for file in files:
                if file.endswith(ext):
                    file_ = os.path.join(root, file)
                    file_list.append(file_)
        return file_list

    @staticmethod
    def video_id_from_filename(fname):
        if len(fname) != 12:
            raise IOError("Wrong format in filename!")
        else:
            return int(fname[4:-4])

    @staticmethod
    def filename_from_video_id(id, metadata):
        pass

    @staticmethod
    def read_json(fname):
        """Read data from a json file."""
        try:
            with open(fname) as file_handler:
                data = json.load(file_handler, object_pairs_hook=OrderedDict)
        except Exception as e:
            print(e)
            raise
        return data

    @staticmethod
    def save_json(data, file, pretty=True):
        """Write data to a json file."""
        with open(file, 'w') as outfile:
            if pretty:
                json.dump(data, outfile, indent=4, sort_keys=True)
            else:
                json.dump(data, outfile)

    @staticmethod
    def extract_frames(file):
        video = cv2.VideoCapture(file["file"])
        video.set(cv2.CAP_PROP_POS_FRAMES, float(file["video"]["anchor_frame"]))
        remaining = file["video"]["total_frames"] - file["video"]["anchor_frame"]  # noqa: E501
        folder = os.path.join("data", str(file["id"]))

        if not utils.check_folder_exists(folder):
            utils.create_folder(folder)

        for i in range(remaining):
            retval, image = video.read()
            if not retval:
                image = np.zeros((720, 1280, 3), dtype=np.uint8)
            

            cv2.imwrite("{folder}/{fn}.png".format(folder=folder, fn=i), image)  # noqa: E501


    @staticmethod
    def hardcoded_values(id):
        corresponding_id, anchor_frame, run, batch, trial = None, None, None, None, None  # noqa: E501
        idx = [None, None, None, 846, 847, 848, 849, 850, 851, 852, 853, 854,
               855, 856, 857, 858, 859, 860, 861, 862, 863, 864, 865, 866, 867,
               868, 869, 870, 871, 872, 873, 874,
               None, None, None, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
               21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35,
               36, 37, 38]

        anchor_frames = [None, None, None, 3324, 2578, 578, 3458, 1418, 2607,
                         2755, 4207, 1846, 942, 2324, 2348, None, 1243, 1664,
                         None, 4293, 4195, 1897, 2803, 6355, 1419, 1435, 1924,
                         6003, 1413, 2022, None, 5100,
                         None, None, None, 2432, 2330, 229, 1800, 1161, 2320,
                         2416, 3926, 1411, 653, 2066, 2591, None, 1923, 1584,
                         None, 4004, 3817, 1574, 2516, 6603, 1178, 1025, 1889,
                         6356, 1073, 1754, None, 5798]

        left = [842, 843, 844, 846, 847, 848, 849, 850, 851, 852, 853, 854,
                855, 856, 857, 858, 859, 860, 861, 862, 863, 864, 865, 866,
                867, 868, 869, 870, 871, 872, 873, 874]
        right = [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
                 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37,
                 38]

        batches = [1, 1, 1, 2, 2, 2, 2, 4, 4, 4, 2, 2, 2, 2, 2, 6, 6, 5, 5, 1,
                   1, 1, 1, 1, 4, 2, 3, 3, 1, 1, 1, 1]

        runs = [1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 2, 1, 1, 1, 3, 3, 3,
                3, 3, 3, 1, 1, 1, 1, 2, 2, 2, 2]

        trials = [1, 2, 3, 1, 2, 3, 3, 1, 2, 3, 1, 2, 3, 4, 1, 1, 1, 2, 1, 1,
                  1, 2, 3, 4, 1, 1, 1, 1, 1, 2, None, 3]

        for _id, elem in enumerate(idx):
            if elem == id:
                anchor_frame = anchor_frames[_id]

        for _id, elem in enumerate(left):
            if elem == id:
                corresponding_id = right[_id]
                batch = batches[_id]
                run = runs[_id]
                trial = trials[_id]

        for _id, elem in enumerate(right):
            if elem == id:
                corresponding_id = left[_id]
                batch = batches[_id]
                run = runs[_id]
                trial = trials[_id]

        return corresponding_id, anchor_frame, run, batch, trial
