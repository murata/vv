import os
import sys
from collections import OrderedDict
import json
import cv2
import numpy as np
from utils import utils

def extract_frames(file, anchor=210, every=150):
    video = cv2.VideoCapture(file)
    video.set(cv2.CAP_PROP_POS_FRAMES, anchor)
    total_frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
    remaining = total_frames - anchor  # noqa: E501
    print("remaining:", remaining, total_frames, anchor)
    folder = os.path.join("data", os.path.basename(file)[:-4])

    if not utils.check_folder_exists(folder):
        utils.create_folder(folder)

    for i in range(0, remaining, every):
        retval, image = video.read()
        if not retval:
            image = np.zeros((720, 1280, 3), dtype=np.uint8)

        cv2.imwrite("{folder}/{fn:05}.png".format(folder=folder, fn=i), image)  # noqa: E501


if __name__ == '__main__':
    print(sys.argv)
    extract_frames(sys.argv[1])
